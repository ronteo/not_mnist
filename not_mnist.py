from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.python.estimator.model_fn import ModeKeys as Modes
import tensorflow as tf
#from tensorflow.contrib.keras.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
# Data sets
DATA_PATH = "/Users/U0115286/git/not_mnist/data/samples/"

tf.logging.set_verbosity(tf.logging.INFO)

SIGNATURE_NAME = "serving_default"

INPUT_TENSOR_NAME = 'inputs'

# Hyper-parameters
# Learning rate for the model
LEARNING_RATE = 0.001


#for prediction
def serving_input_fn():
    inputs = {INPUT_TENSOR_NAME: tf.placeholder(tf.float32, [None, 784])}
    return tf.estimator.export.ServingInputReceiver(inputs, inputs)


def train_input_fn(training_dir):
    return _input_fn(training_dir, batch_size=100)


def eval_input_fn(training_dir):
    return _input_fn(training_dir, batch_size=100)


def _input_fn(training_dir, batch_size=100):
    batch_x, batch_y = ImageDataGenerator(rescale=1. / 255). \
        flow_from_directory(directory=training_dir, target_size=(28, 28), color_mode="grayscale",
                            batch_size=batch_size).next()

    images, labels = tf.convert_to_tensor(batch_x, dtype=tf.float32), tf.convert_to_tensor(batch_y, dtype=tf.float32)

    return {INPUT_TENSOR_NAME: images}, labels


def model_fn(features, labels, mode, params):
    # Logic to do the following:
    # 1. Configure the model via TensorFlow operations
    # 2. Define the loss function for training/evaluation
    # 3. Define the training operation/optimizer
    # 4. Generate predictions
    # 5. Return predictions/loss/train_op/eval_metric_ops in EstimatorSpec object

    # Input Layer
    input_layer = tf.reshape(features[INPUT_TENSOR_NAME], [-1, 28, 28, 1])
    tf.summary.image('image', features[INPUT_TENSOR_NAME], max_outputs=10)


    # Convolutional Layer #1
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=32,
        kernel_size=[5, 5],
        padding='same',
        activation=tf.nn.relu)

    # Pooling Layer #1
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

    # Convolutional Layer #2 and Pooling Layer #2
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=64,
        kernel_size=[5, 5],
        padding='same',
        activation=tf.nn.relu)

    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

    # Dense Layer
    pool2_flat = tf.reshape(pool2, [-1, 7 * 7 * 64])

    dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)

    dropout = tf.layers.dropout(
        inputs=dense, rate=0.4, training=(mode == Modes.TRAIN))

    # Logits Layer
    logits = tf.layers.dense(inputs=dropout, units=10)

    predicted_indices = tf.argmax(input=logits, axis=1)

    # Define operations
    if mode in (Modes.PREDICT, Modes.EVAL):
        probabilities = tf.nn.softmax(logits, name='softmax_tensor')

    if mode in (Modes.TRAIN, Modes.EVAL):
        global_step = tf.train.get_or_create_global_step()
        loss = tf.losses.softmax_cross_entropy(
            labels, logits=logits)
        tf.summary.scalar('OptimizeLoss', loss)



    if mode == Modes.PREDICT:
        predictions = {
            'classes': predicted_indices,
            'probabilities': probabilities
        }

        export_outputs = {
            SIGNATURE_NAME: tf.estimator.export.PredictOutput(predictions)
        }

        # export_outputs: Describes the output signatures to be exported to
        # `SavedModel` and used during serving.
        return tf.estimator.EstimatorSpec(
            mode, predictions=predictions, export_outputs=export_outputs)

    if mode == Modes.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=params["learning_rate"])
        train_op = optimizer.minimize(loss, global_step=global_step)
        return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)



    if mode == Modes.EVAL:

        eval_metric_ops = {
            'accuracy': tf.metrics.accuracy(tf.argmax(labels, axis=1), predicted_indices)
        }

    return tf.estimator.EstimatorSpec(
            mode, loss=loss, eval_metric_ops=eval_metric_ops)


def main():
    # Set model params
    model_params = {"learning_rate": LEARNING_RATE}
    nn = tf.estimator.Estimator(model_fn=model_fn, params=model_params, model_dir="./logdir")

    # Train
    nn.train(input_fn=lambda: train_input_fn(DATA_PATH), steps=50)

    ev = nn.evaluate(input_fn=lambda: train_input_fn(DATA_PATH), steps=1)

    print("Loss: %s" % ev["loss"])
    print("Accuracy: %s" % ev["accuracy"])


if __name__ == "__main__":
    main()
